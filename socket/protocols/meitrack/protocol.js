/**
 * Created by thaigpstracker03 on 6/14/2018 AD.
 */
var strPad = require('string-padding');
var crc16 = require('crc-itu').crc16;
var fs = require('fs');

var utils = require('../_helpers/utils');
var debug = require('../_helpers/debug');
var config = require('./config');
var parser = require('../../../server/applications/gps_service/parsers/gps_box_parser/MeitrackParser');

var protocol = {
    code : {
        'AAA' : 'AUTO_EVENT_REPORT',
        'A71' : 'AUTHORIZED_PHONE_NUMBER_RESULT',
        'A72' : 'LISTEN_IN_PHONE_NUMBER_RESULT',
        'B68' : 'ALARM_SOUND_RESULT',
        'B99' : 'SETTING_EVENT_AUTHORIZED_RESULT',
        'D00' : 'GET_PICTURE',
        'D01' : 'GET_PICTURE_NAME',
        'D02' : 'DELETE_PICTURE',
        'D03' : 'TAKE_PICTURE',
        'C01' : 'DISCONNECT_ENGINE',
        'FC7' : 'OTA_SETTING_SERVER',
        'FC6' : 'OTA_CEHCK_FIRMWARE',
        'FC5' : 'OTA_CHECK_DEVICE_CODE',
    },
    type : {
        // Input Active
        1 : 'INPUT_1_ACTIVE', // SOS Pressed
        2 : 'INPUT_2_ACTIVE',
        3 : 'IGNITION_ON',
        4 : 'INPUT_4_ACTIVE',
        5 : 'INPUT_5_ACTIVE',

        // Input Inactive
        9 : 'INPUT_1_INACTIVE', // SOS Release
        10 : 'INPUT_2_ACTIVE',
        11 : 'IGNITION_OFF',
        12 : 'INPUT_4_ACTIVE',
        13 : 'INPUT_5_ACTIVE',

        // Alarm
        17 : 'LOW_BATTERY',
        18 : 'LOW_EXTERNAL_POWER',
        19 : 'SPEEDING',
        20 : 'ENTER_GEO_FENCE',
        21 : 'EXIT_GEO_FENCE',
        22 : 'EXTERNAL_POWER_ON',
        23 : 'EXTERNAL_POWER_OFF',
        24 : 'NO_GPS_SIGNAL',
        25 : 'GET_GPS_SIGNAL',
        26 : 'ENTER_SLEEP',
        27 : 'EXIT_SLEEP',
        28 : 'GPS_ANTENNA_CUT',
        29 : 'DEVICE_REBOOT',
        30 : 'IMPACT',
        31 : 'HEARTBEAT_REPORT',
        32 : 'HEADING_CHANGE_REPORT',
        33 : 'DISTANCE_INTERVAL_REPORT',
        34 : 'CURRENT_LOCATION_REPORT',
        35 : 'TIME_INTERVAL_REPORT',
        36 : 'TOW_ALARM',
        37 : 'RFID', // card log-in
        39 : 'PICTURE',

        50 : 'TEMP_HIGH',
        51 : 'TEMP_LOW',
        52 : 'FUEL_HIGH',
        53 : 'FUEL_LOW',
        54 : 'FUEL_THEFT',

        86 : 'RFID_LEAVE',// card log-out

        129 : 'RUSH_DECELERATE_ALARM',
        130 : 'RUSH_ACCELERATE_ALARM',
        131 : 'RPM_OVERSPEED_ALARM',
        132 : 'RPM_RECOVERY_ALARM',
        133 : 'IGNITION_ONPARK_OVERTIME_ALARM',
        134 : 'IGNITION_ONPARK_RECOVERY_ALARM',
        135 : 'FATIGUE_DRIVING_ALARM',
        136 : 'OVERTIME_REST_ALARM',
        137 : 'ENGINE_OVERHEAT_ALARM',
        138 : 'SPEED_RECOVERY_ALARM',
        139 : 'MAINTENACE_ALARM',
        140 : 'ENGINE_ERROR_ALARM',
        141 : 'STATUS_ERROR_ALARM',
        142 : 'HEALTH_INSPECT_ALARM',
        143 : 'LOW_FUEL_ALARM',
        144 : 'IGNITION_ON_ALARM',
        145 : 'IGNITION_OFF_ALARM',
        146 : 'CAR_START_ALARM',
        147 : 'CAR_STOP_ALARM',
    },
    response : {
        'LOGIN' : new Buffer(10)
    }
    ,autoCommand : {
        B99_call : 'B99,call,{PHONE},1,72'
        ,B99_cancel_sms : 'B99,SMS,{PHONE},3,1,17,18,19,20,21,23,34,35,36'
        ,B68_alarm_sound : 'B68,{SECOND}'
    }
};
protocol.STOP_BIT = "\r\n";
protocol.START_BIT = "$$";
protocol.COMMAND_START_BIT = "@@";
protocol.WRONG_RFID = [
    "00000001"
    ,00000001
    ,"00000000"
    ,00000000
    ,0
    ,1
    ,"0"
    ,"1"
    ,""
    ,null
];



protocol.generateCommand = function(flag,message,client){
    var start_bit = protocol.COMMAND_START_BIT;
    var stop_bit = protocol.STOP_BIT;
    var flag = 'D';
    var message_length = (message.length + 4);

    var commandCheckSum = new String().concat(start_bit,flag,message_length,message);

    var error_check = protocol.checkSum(commandCheckSum);
    // console.log("CHECKSUM : ",commandCheckSum,checkSum(commandCheckSum),error_check);
    // var data = Buffer.concat([packet_length,protocol_number,command_length,command_flag,content,serial_number]);

    var response = new String().concat(commandCheckSum,error_check,stop_bit);
    return response;
};

protocol.generateCommandWithCustomFlag = function(flag,message,client){
    var start_bit = protocol.COMMAND_START_BIT;
    var stop_bit = protocol.STOP_BIT;
    var message_length = (message.length + 4);

    var commandCheckSum = new String().concat(start_bit,flag,message_length,message);

    var error_check = checkSum(commandCheckSum);
    // console.log("CHECKSUM : ",commandCheckSum,checkSum(commandCheckSum),error_check);
    // var data = Buffer.concat([packet_length,protocol_number,command_length,command_flag,content,serial_number]);

    var response = new String().concat(commandCheckSum,error_check,stop_bit);
    return response;
}


function processTimeIntervalReport(gps_data,client,mydevices){

    if( (gps_data.speed && Math.floor(gps_data.speed) > 150) && (gps_data.satellites && parseInt(gps_data.satellites) < 3 ) ){
        return client;
    }

    if(moment(gps_data.datetime,INPUT_FORMAT).year() > moment().year()){
        __debug("THIS LOG IS WRONG YEAR [SERVER: "+moment().format(DATE_FORMAT)+"] [LOG: "+moment(gps_data.datetime,INPUT_FORMAT).format(DATE_FORMAT)+"] ",client);
        return client;
    }

    client.imei = gps_data.imei || '';
    if (mydevices[client.imei] && mydevices[client.imei].object_user_username){
        client.username = mydevices[client.imei].object_user_username;
    } else {
        client.username = 'unknown';
    }
    if(mydevices[client.imei] && mydevices[client.imei].object_box_id){
        client.box_id = mydevices[client.imei].object_box_id;
    }else{
        client.box_id = '8';
    }


    // console.log(mydevices[client.imei]);

    var ad_value = gps_data.ad.split('|');
    var oil = parseInt(ad_value[0] || '',16) || 0;
    var ad1 = parseInt(ad_value[0] || '',16) || 0;
    var ad2 = parseInt(ad_value[1] || '',16) || 0;
    var ad3 = parseInt(ad_value[2] || '',16) || 0;
    var ad4 = parseInt(ad_value[3] || '',16) || 0;
    var ad5 = parseInt(ad_value[4] || '',16) || 0;
    var position = gps_data.latitude + ',' + gps_data.longitude;
    var speed = Math.floor(gps_data.speed);
    var gsm = Math.round(gps_data.gsm/31*100);
    var fuel = 0;
    if (gps_data.fuel.length == 4){
        var fuel_num = parseInt(gps_data.fuel.substring(0,2),16);
        var fuel_point = parseInt(gps_data.fuel.substring(2,4),16);
        fuel = parseFloat(fuel_num+'.'+fuel_point);
    }

    var temperature = 0;
    var temp_array = gps_data.temperature.split('|');
    for (var i in temp_array){
        var cTemp = temp_array[i];
        if (cTemp.length == 6){
            var temp_index = parseInt(cTemp.substring(0,2),16);
            var temp_num = parseInt(cTemp.substring(2,4),16);
            var temp_point = parseInt(cTemp.substring(4,6),16);
            temperature = parseFloat(temp_num+'.'+temp_point);
        }
        break;
        /**
         * Limit only one temperature
         */
    }

    var message_all = '';
    for (var i in gps_data){
        message_all += gps_data[i]+',';
    }
    var log_row = {
        log_username : client.username,
        log_IMEI : client.imei,
        log_latlng : position,
        log_address : '',
        log_speed : speed,
        log_SAT : gps_data.satellites,
        log_GSM : gsm,
        log_direction : gps_data.course,
        log_HDOP : gps_data.hdop,
        log_BaseID : gps_data.base_id,
        log_oil : oil,
        log_AD1 : ad1,
        log_AD2 : ad2,
        log_AD3 : ad3,
        log_AD4 : ad4,
        log_AD5 : ad5,
        log_State : gps_data.state,
        log_value : str.pad(parseInt(gps_data.state,16).toString(2),16,'0','STR_PAD_LEFT'),
        log_dateserver : moment().format(DATE_FORMAT),
        log_dateupdated :  moment(gps_data.datetime,INPUT_FORMAT).format(DATE_FORMAT),
        log_rfid : gps_data.custom1,
        log_cctv : message_all,
        log_milage : gps_data.milage,
        log_temp : temperature,
        log_fuel : fuel,
        log_port : 6066
    }

    var event_code = gps_data.code;
    // __debug("Event Code: "+event_code+" => "+protocol.type[event_code],client);
    // if(protocol.type[event_code] == 'RFID_LEAVE'){
    //     devices.checkAndClearLicense(log_row.log_IMEI,0,client);
    // }
    // else if(protocol.type[event_code] == 'RFID' || log_row.log_rfid && protocol.WRONG_RFID.indexOf(log_row.log_rfid) == -1){
    //     var rfidData = utils.readRFID(log_row.log_rfid);
    //     var license = (rfidData)? rfidData[7].trim() : null;
    //     var isAllTrack = (rfidData!=null && license!=null)? 'YES' : 'NO';
    //     __debug("CHECK LICENSE DATA ["+log_row.log_rfid+"]=> ALL TRACK ? :  "+isAllTrack,client);
    //     if(rfidData && license){
    //         var prefixName = "";
    //         var licenseArr = license.match(/\d+/g);
    //         if(licenseArr[1] == 1){
    //             prefixName= "MR.";
    //         }else if(licenseArr[1] == 2){
    //             prefixName= "MS.";
    //         }else{
    //             prefixName = rfidData[3];
    //         }
    //
    //         var driverData = {
    //             driver_first_name : prefixName + rfidData[2]
    //             ,driver_last_name : rfidData[1]
    //             ,driver_gender : (prefixName)?(prefixName.toLowerCase() == "mr.")? "Male" : "Female" : null
    //             ,driver_address : rfidData[4]
    //             ,driver_citizen_id : (rfidData[5] && rfidData[5].length > 13)? rfidData[5].substr(-13) : rfidData[5]
    //             ,driver_birth_day : (rfidData[6] && rfidData[6].length>=8)? moment(rfidData[6].substr(-8),"YYYYMMDD").format("YYYY-MM-DD") : null
    //             ,driver_card_expired : (rfidData[6] && rfidData[6].length>=4)? moment("20"+rfidData[6].substr(0,4),"YYYYMM").format("YYYY-MM") : null
    //             ,driver_rfid : license.replace(/\s+/g, '')
    //             ,driver_user_username : client.username
    //             ,driver_add_by : "PROTOCOL"
    //             ,driver_dateadded : moment().format(DATE_FORMAT)
    //         };
    //         devices.insertDriver(driverData,function(id){
    //             processAndUpdatelicense(log_row,license,id,client);
    //         },client);
    //         if(mydevices[client.imei]){
    //             mydevices[client.imei].object_license = (mydevices[client.imei].object_license != license)?license :null;
    //         }
    //     }else if(!rfidData && !license){
    //         if(log_row.log_rfid && typeof log_row.log_rfid.charAt == 'function' && log_row.log_rfid.charAt(log_row.log_rfid.length -1) == '?'){
    //             log_row.log_rfid = log_row.log_rfid.substr(0,log_row.log_rfid.length-1);
    //         }
    //         license = log_row.log_rfid;
    //         devices.getDriver(license.replace(/\s+/g, ''),function(id){
    //             processAndUpdatelicense(log_row,license,id,client);
    //         });
    //         if(mydevices[client.imei]){
    //             mydevices[client.imei].object_license = (mydevices[client.imei].object_license != license)?license :null;
    //         }
    //     }
    // }
    // else{
    //     var engine_state = devices.parseStatusMeitrack(log_row,client.box_id);
    //     if(engine_state == 0 || engine_state == '0'){
    //         devices.checkAndClearLicense(log_row.log_IMEI,engine_state,client);
    //     }
    // }

    // devices.insertLog(client.imei,log_row);
    //
    // if(thgpsDLT && mydevices[client.imei] && parseInt(mydevices[client.imei].object_dlt) == 1){
    //     thgpsDLT.sendLog(client,log_row,mydevices[client.imei]);
    // }
    //
    // if(dataUtils && mydevices[client.imei] && parseInt(mydevices[client.imei].object_scg) == 1){
    //     client = checkDangerZone(log_row,client,mydevices[client.imei]);
    // }

    utils.printLogData(client,log_row);
    // devices.updatePosition(client.imei,{
    //     object_last_position : log_row.log_latlng,
    //     object_last_time : log_row.log_dateupdated
    // });


    client.object_last_position = log_row.log_latlng;
    client.object_last_time  = log_row.log_latlng;


    client.last_course = log_row.log_direction;
    client.last_baseid = log_row.log_BaseID;
    client.last_satellites = log_row.log_SAT;
    client.last_speed = log_row.log_speed;
    client.last_latlng = log_row.log_latlng;


    // if(gps_data.code != 35){
    //     checkLineNotifyAlertFromEventCode(mydevices[client.imei],gps_data.code,log_row);
    // }
    // client = lineNotify.checkAlertFromSetting(client,mydevices[client.imei],log_row);

    // if(mailNotify && log_row){
    //     mailNotify.checkMailNotify(log_row);
    // }


    return client;
}

function checkSum(text){
    var checksum = 0;
    for (var i =0;i<text.length;i++){
        checksum += text.charCodeAt(i);
    }
    var bitsum = (checksum & 0xFF).toString(16).toUpperCase();
    if (bitsum.length == 1) bitsum = "0"+bitsum;
    return bitsum;
}

function responseProtocol(protocol_number,message,serial){
    var start_bit = protocol.START_BIT;
    var stop_bit = protocol.STOP_BIT;
    var message_length = message.length;

    var packet_length = new Buffer([5+message_length]);
    var protocol_number = new Buffer([protocol_number]);
    var content = new Buffer(message_length);
    content.write(message);

    var serial_number = serial;//new Buffer(2); serial_number.writeInt16BE(serial,0);

    var data = Buffer.concat([packet_length,protocol_number,content,serial_number]);
    var error_check = new Buffer(strPad(crc16(data).toString(16),4, '0', strPad.LEFT).toString(16),'hex');

    var response = Buffer.concat([start_bit,data,error_check,stop_bit]);


    // var data_packet = response.slice(2,response.length-4);
    // var crc_bit = strPad(crc16(data_packet).toString(16),4, '0', strPad.LEFT);
    // new Buffer(crc_bit.toString(16),'hex').copy(response,6); // error check 2bit

    return response;
}


function updateResponseCommand(data_array,client){

    if(client.last_command && client.last_command.id){
        var data_order_command = ['flag','imei','command','status'];
        var data_command = {};
        for (var x in data_order_command){
            data_command[data_order_command[x]] = data_array[x] || '';
        }

        //TODO DB : UPDATE COMMAND
        // __debug(data_command,client);
        // var update_command = {
        //     finish_date : moment().format(DATE_FORMAT),
        //     command_status : (data_command["status"])?data_command["status"]:'Finished'
        // };
        // devices.updateCommand(client.last_command.id,update_command);
    }
}

function processAfterTakeImage(data,client){
    var messageText = data.toString();
    var message = messageText.substr(2,messageText.length-7);
    var data_array = message.split(',');
    var data_order = ['flag','imei','command','response'];
    var gps_data = {};
    for (var i in data_order){
        gps_data[data_order[i]] = data_array[i] || '';
    }
    client.imei = gps_data.imei || '';
    if (gps_data.response == 'OK'){
        //TODO COnnect Redis And DB : UPDATE COMMAND
        // var redis = require("redis").createClient(6379, '127.0.0.1',{no_ready_check:false,socket_keepalive:false});
        // redis.get("LASTCOMMAND_"+gps_data.imei,function(err,message){
        //     if (err || !message) {
        //         __debug("<-- GET Picture from unknown command");
        //         // return false;
        //     } else {
        //         client.last_command = JSON.parse(message) || false;
        //         setTimeout(function(){
        //             try {
        //                 var command_message = ","+gps_data.imei+",D00,"+client.last_command.param+",0*";
        //                 var response = protocol.generateCommand(0,command_message,client);
        //                 client.socket.write(response);
        //                 __debug('--> GET PICTURE FROM IMEI : ' + client.imei + ' | ' + response,client);
        //             } catch (ex){console.log(ex);}
        //         }, COMMAND_DELAY);
        //         redis.quit();
        //     }
        // });
    } else {

        __debug('--> TAKE PHOTO ERROR IMEI : ' + client.imei + ' | ' + gps_data.response,client);
    }
    return client;
}


function clearCommand(imei,client){
    //TODO Connect Redis
    // var redis = require("redis").createClient(6379, '127.0.0.1',{no_ready_check:false,socket_keepalive:false});
    // redis.del("LASTCOMMAND_"+imei);
    // client.inprogress = false;
    // redis.quit();
    // return client;
}


function processCutEngine(data,client,mydevices){
    var messageText = data.toString();
    var message = messageText.substr(2,messageText.length-7);
    var data_array = message.split(',');
    var data_order = ['flag','imei','command','result'];
    var gps_data = {};

    for (var i in data_order){
        gps_data[data_order[i]] = data_array[i] || '';
    }
    client.imei = gps_data.imei || '';
    client.inprogress = false;
    //TODO DB : UPDATE COMMAND
    // var update_command = {
    //     finish_date : moment().format(DATE_FORMAT),
    //     command_status : gps_data.result
    // };
    // devices.updateCommand((client.last_command && client.last_command.id) || 0,update_command);

    //Clear command
    client = clearCommand(gps_data.imei,client);
    return client;
}

function processDeleteImage(data,client,mydevices){
    var messageText = data.toString();
    var message = messageText.substr(2,messageText.length-5);
    var data_array = message.split(',');
    var data_order = ['flag','imei','command','result'];
    var gps_data = {};
    for (var i in data_order){
        gps_data[data_order[i]] = data_array[i] || '';
    }
    client.imei = gps_data.imei || '';
    //Clear command
    client = clearCommand(gps_data.imei,client);
    __debug("Remove Image "+messageText,client);
    return client;
}

function processCaptureImage(data,client,mydevices){
    var messageText = data.toString();
    var message = messageText.substr(2,messageText.length-5);
    var data_array = message.split(',');
    var data_order = ['flag','imei','command','name','total','index'];
    var gps_data = {};
    for (var i in data_order){
        gps_data[data_order[i]] = data_array[i] || '';
    }
    client.imei = gps_data.imei || '';


    var start_data_index = 0;
    var end_data_index = data.length - 5;
    var split_index = 0;
    for (var i in data){
        if (data[i] == 0x2c) {
            split_index++;
            if (split_index == 6) {
                start_data_index = parseInt(i) +1;
                break;
            }
        }
    }
    // console.log("Image Size : ",end_data_index,start_data_index,end_data_index-start_data_index);
    var img_data = data.slice(start_data_index,end_data_index);
    // client.username = mydevices[client.imei].user_username;
    //TODO SAVE FILE TO SERVER
    // var img_path = config.image_path + gps_data.imei + '/';
    // if (!fs.existsSync(img_path)) {
    //     fs.mkdirSync(img_path, 0777);
    // }
    // __debug('Write image '+(parseInt(gps_data.index)+1)+'/'+gps_data.total+' to : '+img_path+gps_data.name,client);
    // fs.writeFile(img_path+gps_data.name, img_data,{
    //     encoding : 'binary',
    //     flag : (gps_data.index == 0? 'w' : 'a')
    // },function (err) {
    //     if (err) throw err;
    // });
    // console.log(gps_data);
    //TODO CONNECT REDIS AND UPDATE COMMAND
    // var redis = require("redis").createClient(6379, '127.0.0.1',{no_ready_check:false,socket_keepalive:false});
    // redis.get("LASTCOMMAND_"+gps_data.imei,function(err,message){
    //     if (err || !message) {
    //         __debug("<-- GET Picture from unknown command | imei: "+gps_data.imei+", name: ",+gps_data.name);
    //         // return false;
    //     } else {
    //         client.last_command = JSON.parse(message) || false;
    //         if (gps_data.index < gps_data.total -1 && gps_data.index % 8 == 7 && gps_data.index != 0) {
    //             client.inprogress = true;
    //             setTimeout(function(){
    //                 var command_message = ","+gps_data.imei+","+gps_data.command+","+gps_data.name+","+(parseInt(gps_data.index)+1)+"*";
    //                 var response = protocol.sendCommand(0,command_message,client);
    //                 client.socket.write(response);
    //                 __debug('--> SEND COMMAND TO IMEI : ' + client.imei + ' | ' + response,client);
    //             },COMMAND_DELAY);
    //
    //         } else if (gps_data.index == gps_data.total -1) {
    //             client.inprogress = false;
    //             var update_command = {
    //                 finish_date : moment().format(DATE_FORMAT),
    //                 command_status : 'Received Picture'
    //             };
    //             devices.updateCommand((client.last_command && client.last_command.id) || 0,update_command);
    //             //Clear command
    //             client = clearCommand(gps_data.imei,client);
    //             setTimeout(function(){
    //                 var command_message = ","+gps_data.imei+",D02,"+gps_data.name+"|*";
    //                 var response = protocol.sendCommand(0,command_message,client);
    //                 client.socket.write(response);
    //                 __debug('--> SEND DELETE COMMAND TO IMEI : ' + client.imei + ' | ' + response,client);
    //             },COMMAND_DELAY);
    //         }
    //     }
    //     redis.quit();
    // });
    return client;
}




function processAndUpdatelicense (log_row,license,driver,client){

    //TODO Recoding
    // var engine_state = devices.parseStatusMeitrack(log_row,client.box_id);
    var isLicense = utils.checkRFIDIsLicense(license);
    // __debug("ENGINE_STATE ==> " + engine_state + " RFID=>" + license + " IS LICENSE => "+isLicense,client);
    //
    // if(engine_state == 0 || engine_state == '0') {
    //     devices.checkAndClearLicense(log_row.log_IMEI, engine_state, client);
    // }
    // else if(isLicense){
    //     var data = {
    //         object_license : license
    //     };
    //     if(driver){
    //         data.object_driver = driver;
    //     }
    //     devices.updateLicense(log_row.log_IMEI,engine_state,data,client);
    // }
}


function processAuthorizedPhoneNumber(data_array,client,mydevices){
    try{

        var data_order_command = ['flag','imei','command','status'];
        var data_command = {};
        for (var x in data_order_command){
            data_command[data_order_command[x]] = data_array[x] || '';
        }
        __debug("[A71 RESULT PROCESS]=> GET A71 RESULT COMMAND => "+data_command.status,client);
        if(data_command.imei && data_command.status == "OK"){
            /* AUTO SEND B99 Event FOR CALL AND SMS */
            __debug("[A71 RESULT PROCESS]=> GET LAST A71 COMMAND",client);
            devices.findLastCommandByCommandCodeAndIMEI(data_command.imei,"A71",function(command){

                // console.log("[A71 RESULT PROCESS]=> ", "LAST A71 COMMAND",command);
                if(command && command.command_name && (typeof command.command_name.split == 'function')){

                    // __debug("[A71 RESULT PROCESS]=> FOUND LAST A71 COMMAND",client);
                    try{

                        var cmd = command.command_name.split(",");
                        // console.log("[A71 RESULT PROCESS]=> ", "LAST A71 COMMAND",cmd);
                        if(cmd && cmd.length > 0){
                            var second = 3;
                            for(var i=1;i<cmd.length;i++){
                                var d = cmd[i];
                                if(d && (typeof d.length !== 'undefined') && d.length >= 5){


                                    // __debug("[A71 RESULT PROCESS]=>  SET TIME OUT FOR SEND B99 TO "+d,client);
                                    setTimeout(function(client,imei,phoneNumber){
                                        /* AUTO SEND B99 Event FOR CALL */
                                        if(client && client.socket && (typeof client.socket.write == 'function') && imei && phoneNumber){
                                            var cmdDetail = protocol.autoCommand.B99_call;
                                            cmdDetail = cmdDetail.replace("{PHONE}",phoneNumber);
                                            var command_message = ','+imei+','+cmdDetail;
                                            command_message += '*';
                                            __debug("[A71 RESULT PROCESS]=> AUTO SENDING B99 TO "+imei+" FOR SETUP 2 WAY CALL  "+phoneNumber+ " ("+command_message+") ",client);
                                            var response = protocol.sendCommandWithCustomeFlag("A",command_message,client);
                                            client.socket.write(response);
                                        }
                                    }.bind(this,client,command.command_object_imei,d),1000 * second /*  Delay Execute */)


                                    second = second + 3;
                                }
                            }
                        }
                    }
                    catch (e){
                        __debug("[A71 RESULT PROCESS]=> AUTHORIZED_PHONE_NUMBER_RESULT ERROR: "+e,client);
                    }

                }
                else{

                    __debug("[A71 RESULT PROCESS]=> LAST A71 COMMAND NOT FOUND",client);
                }
            });
        }
        updateResponseCommand(data_array,client);
    }
    catch (e){
        console.log("AUTO SEND B99 for "+command.command_object_imei+" ERROR: "+e);
    }
}

function processSettingEventAuthorizedResult(data_array,client,mydevices){
    var data_order_command = ['flag','imei','command','field','operation','phone_number'];
    var data_command = {};
    for (var x in data_order_command){
        data_command[data_order_command[x]] = data_array[x] || '';
    }
    // console.log("processSettingEventAuthorizedResult",data_command);
    if(data_command.flag && data_command.flag[0] == "A" && data_command.imei && data_command.field != 'Error') {
        var object = mydevices[data_command.imei];
        if (object) {

            var sentCommandData = {
                command_user_id : object.object_user_username
                ,command_name : ""
                ,command_status : "OK"
                ,command_object_imei : object.object_IMEI
                ,finish_date : moment().format(DATE_FORMAT)
            };
            switch (data_command.field) {
                case 'call' : { //COMMAND RESULT FLAG FOR AUTO B99 2 WAY FOR CALL
                    var msgDetail = " Auto Send setup 2 way communication for CALL to " + data_command.phone_number;
                    sentCommandData.command_name = msgDetail;
                    devices.insertSendCommand(sentCommandData, function (isSuccess, rows) {
                    });

                    setTimeout(function(client,imei,phoneNumber){
                        /* AUTO SEND B99 Event FOR  SMS */
                        if(client && client.socket && (typeof client.socket.write == 'function') && imei && phoneNumber){
                            var cmdDetail = protocol.autoCommand.B99_cancel_sms;
                            cmdDetail = cmdDetail.replace("{PHONE}",phoneNumber);
                            var command_message = ','+imei+','+cmdDetail;
                            command_message += '*';
                            __debug("[A71 RESULT PROCESS]=> AUTO SENDING B99 TO "+imei+" FOR CANCEL SMS "+phoneNumber + " ("+command_message+") ",client);
                            var response = protocol.sendCommandWithCustomeFlag("A",command_message,client);
                            client.socket.write(response);

                        }
                    }.bind(this,client,object.object_IMEI,data_command.phone_number),1000 * 9 /* 3 Second Delay */)

                }
                    break;
                case'SMS':{//COMMAND RESULT FLAG FOR AUTO B99 2 WAY FOR SMS

                    var msgDetail = " Auto Send setup 2 way communication for SMS to " + data_command.phone_number;
                    sentCommandData.command_name = msgDetail;
                    devices.insertSendCommand(sentCommandData, function (isSuccess, rows) {
                    });

                }
                    break;
            }
        }
    }
    else{
        __debug("[processSettingEventAuthorizedResult] NOT COMPLETE => [FLAG="+data_command.flag+"] [COMMAND="+data_command.command+"] [FIELD="+data_command.field+"] [OPT="+data_command.operation+"] [PHONE="+data_command.phone_number+"]",client);
    }
}

function processOTACheckDeviceCodeResult(data_array,buffer,client,mydevices){
    var data_order_command = ['flag','imei','command','status'];
    var data_command = {};
    for (var x in data_order_command){
        data_command[data_order_command[x]] = data_array[x] || '';
    }

    __debug(buffer,client);

    if(data_command.status != 'NOT'){
        var version_code = buffer.slice(-7,-5);
        data_command.status = getHex(version_code);
    }


    if(client.last_command && client.last_command.id){

        // __debug(data_command,client);
        var update_command = {
            finish_date : moment().format(DATE_FORMAT),
            command_status : data_command.status
        };
        devices.updateCommand(client.last_command.id,update_command);
    }

}

function processOTACheckFirmWareResult(data_array,buffer,client,mydevices){

    var data_order_command = ['flag','imei','command','status'];
    var data_command = {};
    for (var x in data_order_command){
        data_command[data_order_command[x]] = data_array[x] || '';
    }

    __debug(buffer,client);

    if(data_command.status != 'OK'){
        switch (data_command.status){
            case 1 : case '1':{
            data_command.status = 'OTA file is Same Device \'s version.'
        }
            break;
            case 2 : case '2':{
            data_command.status = 'OTA file does not match device. Device may be down. If want to update, do next step.'
        }
            break;
        }
    }


    if(client.last_command && client.last_command.id){

        // __debug(data_command,client);
        var update_command = {
            finish_date : moment().format(DATE_FORMAT),
            command_status : data_command.status
        };
        devices.updateCommand(client.last_command.id,update_command);
    }

}

function processOTASettingServerResult(data_array,buffer,client,mydevices){
    updateResponseCommand(data_array,client);
}


// function checkLineNotifyAlertFromEventCode(object,eventCode,log){
//
//     try{
//         if(object && object.object_box_id && eventCode){
//             var token = lineNotify.userToken[object.object_user_username];
//             var isEnabled = lineNotify.isUserConfigEnableUByEventCode(object,eventCode);
//
//             console.log("CHECK ALERT FROM EVENT CODE => "+object.object_user_username+" HAS TOKEN ="+((token)? "Y" : "N" )+ " | ENABLE ="+((isEnabled)? "Y" : "N") );
//
//             if(token && isEnabled){
//                 var alert_type = null;
//                 var paired_data = lineNotify.getRequiredData(object,log);
//                 switch (parseInt(eventCode)){
//                     case 1 : { // SOS
//                         alert_type = lineNotify.alertType.ALERT_TYPE_SOS;
//                     }
//                         break
//                     case 3 : { // IGNITION ON
//                         alert_type = lineNotify.alertType.ALERT_TYPE_ENGINE_ON;
//                     }
//                         break
//                     case 11 : { // IGNITION OFF
//                         alert_type = lineNotify.alertType.ALERT_TYPE_ENGINE_OFF;
//                     }
//                         break
//                     case 23 : { // EXTERNAL BATTERY CUT
//                         alert_type = lineNotify.alertType.ALERT_TYPE_UNPLUG;
//                     }
//                         break
//                     case 24 : { // LOST GPS SIGNAL
//                         alert_type = lineNotify.alertType.ALERT_TYPE_GPS_SIGNAL_LOST;
//                     }
//                         break
//                     case 25 : { // GET GPS SIGNAL
//                         alert_type = lineNotify.alertType.ALERT_TYPE_GPS_SIGNAL_GET;
//                     }
//                         break
//                     case 37 : { // RFID
//                         alert_type = lineNotify.alertType.ALERT_TYPE_SWIPE_RFID_CARD;
//                         paired_data.rfid = object.license;
//                     }
//                         break
//                     case 54 : { // FUEL THEFT
//                         alert_type = lineNotify.alertType.ALERT_TYPE_FUEL_THEFT;
//                     }
//                         break
//                     case 129 : { // HARSH BREAK
//                         alert_type = lineNotify.alertType.ALERT_TYPE_HARSH_BREAK;
//                     }
//                         break
//                     // case 131 : { // OVER SPEED
//                     //     alert_type = lineNotify.alertType.ALERT_TYPE_OVER_SPEED;
//                     //     paired_data.speed = log.log_speed;
//                     //     paired_data.speed_limit = object.object_overspeed;
//                     // }
//                     // break
//                     case 133 : { // OVER PARKING TIME
//                         alert_type = lineNotify.alertType.ALERT_TYPE_OVER_PARK;
//                     }
//                         break
//
//                 }
//                 if(alert_type != null){
//                     __debug("SEND LINE NOTIFY WITH ALERT "+alert_type);
//                     lineNotify.sendNotifyAlertByToken(token,alert_type,paired_data);
//                 }
//             }
//         }
//     }
//     catch (e){
//
//         console.log("Exception : [checkLineNotifyAlertFromEventCode] ",e);
//     }
// }

// function sendAlarmSound(client,second){
//     if(client && client.imei){
//         var cmd = protocol.autoCommand.B68_alarm_sound;
//         second = second || 10;
//         cmd = cmd.replace('{SECOND}',second);
//         var command_message = ','+client.imei+','+cmd+'*';
//         var response = protocol.sendCommand(0,command_message,client);
//         __debug('Send Alarm Sound for '+second+ ' seconds',client);
//         client.socket.write(response);
//     }
// }

// function checkDangerZone(log_row,client,object){
//
//     if(client && log_row && object){
//
//         var danger_poi_scg = dataUtils.checkIsInSCGDangerPoi(log_row,object);
//         if((!client.is_in_danger_poi || (log_row.log_speed == 0)) && danger_poi_scg && danger_poi_scg.is_in == true){
//             client.is_in_danger_poi = true;
//             __debug("Is in Danger Poi : "+(danger_poi_scg.data.poi_name || ''),client);
//             sendAlarmSound(client,30);
//             var line_notify_token = lineNotify.userToken[object.object_user_username];
//             if(line_notify_token){
//                 var paired_data = lineNotify.getRequiredData(object,log_row);
//                 paired_data.poi_name = danger_poi_scg.data.poi_name || '';
//                 paired_data.is_poi_scg = true;
//                 lineNotify.sendNotifyAlertByToken(line_notify_token,lineNotify.alertType.ALERT_TYPE_DANGER_POI,paired_data);
//
//             }
//         }
//         else{
//             client.is_in_danger_poi = false;
//         }
//     }
//
//     return client;
// }

module.exports = protocol;
