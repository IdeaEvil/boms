/**
 * Created by thaigpstracker03 on 6/17/2018 AD.
 */

var geolib = require('geolib');

exports.calculateDistanceBetweenPoint = function(start,end){

    if(start && typeof start == 'string' && end && typeof end == 'string'){

        var locStart = start.split(',');
        var locEnd = end.split(',');

        if(locStart.length ==2 && locEnd.length ==2){
            var src = {
                latitude : locStart[0],
                longitude : locStart[1]
            };
            var dest = {
                latitude : locStart[0],
                longitude : locStart[1]
            };

            var dis = geolib.getDistance(src,dest);

            return dis; // unit Metre.
        }

    }
    return NaN;

};

exports.parseGeocodeToShortAddr =function(geocode){
    var short = [];
    if(geocode){
        if(geocode.tambon && geocode.tambon.indexOf('ตำบล') != -1){
            geocode.tambon = geocode.tambon.replace(/ตำบล/g,'');
        }
        if(geocode.aumphur && geocode.aumphur.indexOf('อำเภอ') != -1){
            geocode.aumphur = geocode.aumphur.replace(/อำเภอ/g,'');
        }
        if(geocode.province && geocode.province.indexOf('จังหวัด') != -1){
            geocode.province = geocode.province.replace(/จังหวัด/g,'');
        }
        if(geocode.kwaeng && geocode.kwaeng.indexOf('แขวง') != -1){
            geocode.kwaeng = geocode.kwaeng.replace(/แขวง/g,'');
        }
        if(geocode.ket && geocode.ket.indexOf('เขต') != -1){
            geocode.ket = geocode.ket.replace(/เขต/g,'');
        }
        var consist = {
            'tambon' : 'ต.',
            'aumphur' : 'อ.',
            'province' : 'จ.',
            'kwaeng' : 'แขวง',
            'ket' : 'เขต'
        };
        for(var key in consist){
            if(geocode[key]){
                var txt = consist[key];
                if(geocode[key].indexOf(txt) == -1){
                    geocode[key] = txt+geocode[key];
                }
                short.push(geocode[key]);
            }
        }
    }
    return short.join(' ');
};

exports.parseGeocodeToShortAddrEn =function(geocode){
    var short = [];
    if(geocode){
        if(geocode.tambon_en && geocode.tambon_en.toString().toLowerCase().indexOf('tambon') != -1){
            geocode.tambon_en = geocode.tambon_en.replace(/tambon/g,'');
        }
        if(geocode.aumphur_en && geocode.aumphur_en.indexOf('amphoe') != -1){
            geocode.aumphur_en = geocode.aumphur_en.replace(/amphoe/g,'');
        }
        if(geocode.province_en && geocode.province_en.indexOf('chang wat') != -1){
            geocode.province_en = geocode.province_en.replace(/chang wat/g,'');
        }
        // if(geocode.kwaeng_en && geocode.kwaeng_en.indexOf('kwaeng') != -1){
        //     geocode.kwaeng_en = geocode.kwaeng_en.replace(/kwaeng/g,'');
        // }
        // if(geocode.ket_en && geocode.ket_en.indexOf('ket') != -1){
        //     geocode.ket_en = geocode.ket_en.replace(/ket/g,'');
        // }
        var consist = {
            'tambon' : '',
            'aumphur' : '',
            'province' : '',
            'kwaeng' : '',
            'ket' : ''
        };
        for(var key in consist){
            if(geocode[key]){
                var txt = consist[key];
                if(geocode[key].indexOf(txt) == -1){
                    geocode[key] = txt+geocode[key];
                }
                short.push(geocode[key]);
            }
        }
    }
    return short.join(' ');
};

exports.generateTimeDurationTextFromSecond = function(second,use,zeros,locale){
    locale = (locale)? 'th' : locale;
    var periodText = null;

    if(locale == "th"){
        periodText = ['ปี', 'เดือน', 'สัปดาห์', 'วัน', 'ชั่วโมง', 'นาที', 'วินาที'];
    }else{
        periodText = ['Year', 'Month', 'Week', 'Day', 'Hour', 'Minute', 'Second'];
    }

    var periods = {};
    periods[periodText[0]] = 31556926;
    periods[periodText[1]] = 2629743;
    periods[periodText[2]] = 604800;
    periods[periodText[3]] = 86400;
    periods[periodText[4]] = 3600;
    periods[periodText[5]] = 60;
    periods[periodText[6]] = 1;

    second = parseFloat(second);

    var segments = {};
    for(var pr in periods){
        var divider = periods[pr];

        if(use && use.indexOf(pr) === -1){
            continue;
        }
        var count = parseFloat(second/ divider);
        if(count == 0 && !zeros){
            continue;
        }
        segments[pr] = count;
        second = second % divider;
    }
    var durationTextArray = [];
    for(var pr in segments){
        var val = segments[pr];
        durationTextArray.push(val + ' ' + pr);
    }

    return durationTextArray.join(' ');
};