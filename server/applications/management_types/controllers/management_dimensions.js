var utils = require('../../../../helpers/utils');
var infomations_dimensions = require('../models/infomations_dimensions.model');
var resMsgs = require('../../management_messages/controllers/Management_Messages');
var mongo = require('mongodb');


exports.onQuery_Dimensions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Dimensions";
    var query = {};
    query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_dimensions
            .find(query)
            .lean()
            .exec(function (err, doc) {
               for(i=0; i<doc.length; i++){
                   delete doc[i].update;
                   delete doc[i].create;
                   delete doc[i].__v
               }

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCreate_Dimensions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Dimensions";
    try {

        var create = {};
        create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.dimensions != null) ? data.dimensions = request.body.dimensions : null;

        data.create = create;

        var infomations_dimension = new infomations_dimensions(data);
        infomations_dimension.save(function (err, doc) {
            delete doc.update;
            delete doc.create;
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_Dimensions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_Dimensions";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.dimensions != null) ? data.dimensions = request.body.dimensions : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomations_dimensions
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                delete doc.update;
                delete doc.create;
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCheckDimenPrice =function (request, response, next) {

    var query = {};
   query._id = new mongo.ObjectID("5bc1f39d2ea3b908f9d99bf2");
    // (company != null) ? query.company = new mongo.ObjectID(company) : null;
    var total_dimension = request.body.price;

    infomations_dimensions
        .findOne(query)
        .lean()
        .exec(function (err, doc) {
            if (err) {
                res(true,nil);
            } else {
                if (doc != null) {
                    delete doc.update;
                    delete doc.create;

                    var dimen = doc.dimensions;
                    var dimens = dimen[0];
                    for(i=0; i<dimen.length; i++){
                        if(total_dimension >= dimen[i].dimension){
                            dimens = dimen[i];
                        }
                    }

                    console.log(dimens);


                    // res(false,doc);
                } else {
                    // res(true,nil);
                }
        }
    });
};