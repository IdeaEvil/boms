'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_DimensionsSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    dimensions          : [{
        dimension       : {type: Number, default: 0},
        price           : {type: Number, default: 0},
    }],
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, required: true}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, required: true}
    }]
});

var Infomation_Dimensions = mongoose.model('Infomation_Dimensions',Infomation_DimensionsSchema, 'Infomations_Dimensions');
module.exports = Infomation_Dimensions;