'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_transportation_positionsSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    position            : [{
        title           : {type: String, default:null},
        description     : {type: String, default: null},
        contact         : {
            name        : {type: String, default:null},
            msisdn      : {type: String, default:null},
            note        : {type: String, default:null},
        },
        locations       : {
            address     : {type: String, default:null},
            lat         : {type: String, default:0.0},
            long        : {type: String, default:0.0},
        },
        addon           : [{type: Schema.ObjectId, ref: 'Job_Services'}],

        controll        : {
            start       : {type: Date, default:null},
            end         : {type: Date, default:null},
            images      : [{
                image   : {type: String, default:null},
            }],
            signature   : {type: String, default:null},
            note        : {type: String, default:null},
            locations   : {
                lat     : {type: String, default:0.0},
                long    : {type: String, default:0.0},
            }
        },
        products        : [{
            name        : {type: String, default:null},
            taxid       : {type: String, default:null},
            number      : {type: Number, default:0.0},
            checker     : {type: Boolean, default:true},
        }],
    }],

});

var infomation_transportation_positions = mongoose.model('infomation_transportation_positions', Infomation_transportation_positionsSchema, 'infomation_transportation_positions');
module.exports = infomation_transportation_positions;