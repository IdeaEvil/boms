var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var multers = require('../../../../helpers/multer');

var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');
var format = require('string-format');
format.extend(String.prototype, {});

var distant = require('../../../../helpers/google/util_distance');
var infomation_transportations = require('../models/infomation_transportations.models');



exports.onSumDimension = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreateJobs";

    items = request.body.transportation_position;
    var total_distant = 0;

    console.log(items.length);
    var total = [];
    if(items.length > 1){
        for (i in items){
            if(i < (items.length-1)){
                var origin = "{0},{1}".format(items[i].locations.lat,items[i].locations.long);
                var index  = parseInt(i)+1;
                var destination = "{0},{1}".format(items[index].locations.lat,items[index].locations.long);

                distant.onDistance(origin, destination, function (data) {
                    total.push(data.distanceValue);

                    if(total.length == i){
                        for(j in total){
                            total_distant = total_distant+total[j];
                        }
                        request.body.total_distance = total_distant;
                        console.log(total_distant);
                        return next();

                    }
                });

            }
        }
    }else{
        request.server.total_distance = 0;
    }
};

exports.onTransportations = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onTransportations";

    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomation_transportations
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        delete doc.create;
                        delete doc.update;

                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onTransportationsById = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onLocationstransportationsById";

    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomation_transportations
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        delete doc.create;
                        delete doc.update;

                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onLocationstransportationsById = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onLocationstransportationsById";

    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomation_transportations
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        delete doc.company;
                        delete doc.partner;
                        delete doc.ref;
                        delete doc.cars;
                        delete doc.type;
                        delete doc.total_distance;
                        delete doc.total_price;
                        delete doc.total_dimension;
                        delete doc.promotions;
                        delete doc.payment_channel;
                        delete doc.start;
                        delete doc.enable;
                        delete doc.transportation_position;
                        delete doc.create;
                        delete doc.update;

                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdateLocationstransportationsById = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateLocationstransportationsById";

    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var location = {};
        (request.body.location != null)? location = request.body.location : null;

        var push = {};
        push.location =  location;
        data.$push = push;

        infomation_transportations
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        delete doc.company;
                        delete doc.partner;
                        delete doc.ref;
                        delete doc.cars;
                        delete doc.type;
                        delete doc.total_distance;
                        delete doc.total_price;
                        delete doc.total_dimension;
                        delete doc.promotions;
                        delete doc.payment_channel;
                        delete doc.start;
                        delete doc.enable;
                        delete doc.transportation_position;
                        delete doc.create;
                        delete doc.update;

                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onCreateTransportaions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreateTransportaions";

    try {

        var data = {};

        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.partner != null) ? data.partner = request.body.partner : null;

        (request.body.ref != null) ? data.ref = request.body.ref : null;
        (request.body.cars != null) ? data.cars = request.body.cars : null;
        (request.body.type != null) ? data.v = request.body.type : null;
        // (request.body.job_start != null) ? data.job_start = request.body.job_start : null;

        // (request.body.enable != null) ? data.enable = request.body.enable : null; // for system

        (request.body.total_distance != null) ? data.total_distance = request.body.total_distance : null; // for system
        (request.body.total_price != null) ? data.total_price = request.body.total_price : null; // for system
        (request.body.total_dimension != null) ? data.total_dimension = request.body.total_dimension : null;

        (request.body.promotions != null) ? data.promotions = request.body.promotions : null;
        (request.body.payment_channel != null) ? data.payment_channel = request.body.payment_channel : null;

        (request.body.start != null) ? data.start = request.body.start : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        (request.body.transportation_position != null) ? data.transportation_position = request.body.transportation_position : null;

        (request.body.benefit != null) ? data.benefit = request.body.benefit : null;
        (request.body.workers != null) ? data.workers = request.body.workers : null;

        var create = {};
        create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        data.create = create;

        var infomation_transportation = new infomation_transportations(data);
        infomation_transportation.save(function (err, doc) {
            delete doc.update;
            delete doc.create;
            // delete doc.benefit;
            // delete doc.payment;
            // delete doc.dimension;

            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                console.log(err);
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog('checkVersion', request, startTime, res, err)
                });
            }
        });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.on๊UpdateTransportaions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "on๊UpdateTransportaions";

    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.ref != null) ? data.ref = request.body.ref : null;
        (request.body.car != null) ? data.car = request.body.car : null;
        (request.body.type != null) ? data.v = request.body.type : null;
        // (request.body.job_start != null) ? data.job_start = request.body.job_start : null;

        // (request.body.enable != null) ? data.enable = request.body.enable : null; // for system

        (request.body.total_distance != null) ? data.total_distance = request.body.total_distance : null; // for system
        (request.body.total_price != null) ? data.total_price = request.body.total_price : null; // for system
        (request.body.total_dimension != null) ? data.total_dimension = request.body.total_dimension : null;

        (request.body.promotions != null) ? data.promotions = request.body.promotions : null;
        (request.body.payment_channel != null) ? data.payment_channel = request.body.payment_channel : null;

        (request.body.start != null) ? data.start = request.body.start : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        (request.body.job_position != null) ? data.job_position = request.body.job_position : null;

        (request.body.benefit != null) ? data.benefit = request.body.benefit : null;
        (request.body.workers != null) ? data.workers = request.body.workers : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;


        infomation_transportations
            .findOneAndUpdate(query, data, { new: true })
            .lean()
            .exec(function (err, doc) {
                delete doc.update;
                delete doc.create;
                delete doc.benefit;
                if(!err){
                    resMsgs.onMessage_Response(0,20000,function(res){
                        var resData = res;
                        resData.data = doc;
                        response.status(200).json(resData);
                        utils.writeLog(command, request, startTime, resData, err)
                    });
                }else{
                    resMsgs.onMessage_Response(0,50003,function(res){
                        response.status(500).json(res);
                        utils.writeLog('checkVersion', request, startTime, res, err)
                    });
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

