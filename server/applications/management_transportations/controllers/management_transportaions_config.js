var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var multers = require('../../../../helpers/multer');

var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var infomation_transportations_config = require('../models/infomation_transportations_config.models');


exports.onConfigsAll = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onConfigsAll";
    try {

        var query = {};

        infomation_transportations_config
            .find(query)
            .lean()
            .exec(function (err, doc) {

                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    console.log(doc);
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onConfigByCompany = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onConfigsAll";
    try {

        var query = {};
        (request.body.company != null) ? query.company = request.params.id : null;

        infomation_transportations_config
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onCreateConfigs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreateTransportaions";

    try {
        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.support != null) ? data.support = request.body.support : null;

        var infomation_transportations_configs = new infomation_transportations_config(data);
        infomation_transportations_configs.save(function (err, doc) {

            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                console.log(err);
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog('checkVersion', request, startTime, res, err)
                });
            }
        });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUpdateConfigsSupportDimension = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;


        var data = {$push : {"support.$.dimension":request.body.dimension}};

        infomation_transportations_config
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onDeleteConfigsSupportDimension = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;

        var data = {$pull : {"support.$.dimension":request.body.dimension}};

        infomation_transportations_config
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUpdateConfigsSupportDistances = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;


        var data = {$push : {"support.$.distances":request.body.distances}};

        infomation_transportations_config
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onDeleteConfigsSupportDistances = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;

        var data = {$pull : {"support.$.distances":request.body.distances}};

        infomation_transportations_config
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}
