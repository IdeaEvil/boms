var utils = require('../../../../helpers/utils');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');
var profile = require('../../management_accounts/controllers/management_profile');

var infomations_companys = require('../models/infomations_companys.models');

exports.onCompanys = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomations_companys
            .find(query)
            .populate("users.by","username userinfo enable")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onCompanyByUser = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners";
    try {
        var query = {};
        (request.params.user != null) ? query['users.by'] = new mongo.ObjectID(request.params.user) : null;

        infomations_companys
            .find(query)
            .populate("users.by","username userinfo enable")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCompanys_Create = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Create";
    try {
        var create = {};
        (request.body.server_by != null)? create.by = request.body.server_by : null;
        // create.date = startTime;

        var data = {};
        (request.body.title != null)? data.title = request.body.title : null;
        (request.body.description != null)? data.description = request.body.description : null;
        // (request.body.image != null)? data.image = request.body.image : null;
        (request.body.contact != null)? data.contact = request.body.contact : null;
        (request.body.config != null)? data.config = request.body.config : null;
        (request.body.users != null)? data.users = request.body.users : null;
        (request.body.enable != null)? data.enable = request.body.enable : null;
        data.create = create;

        var infomations_company = new infomations_companys(data);
        infomations_company.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                console.log(err);
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    } catch (err) {
        console.log(2);
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCompanys_Update = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.title != null)? data.title = request.body.title : null;
        (request.body.description != null)? data.description = request.body.description : null;
        // (request.body.image != null)? data.image = request.body.image : null;
        (request.body.contact != null)? data.contact = request.body.contact : null;
        (request.body.config != null)? data.config = request.body.config : null;
        (request.body.enable != null)? data.enable = request.body.enable : null;

        var users = [];
        (request.body.users != null)? users.$each = request.body.users : null;



        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        (request.body.users != null)? push.users = users : null;
        data.$push = push;

        infomations_companys
            .findOneAndUpdate(query,data,{new:true})
            .populate("users.by","username userinfo enable")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCompanys_Add_User = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var users = {};
        (request.body.users != null)? users.$each = request.body.users : null;


        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        (request.body.users != null)? push.users = users : null;
        data.$push = push;

        infomations_companys
            .findOneAndUpdate(query,data,{new:true})
            .populate("users.by","username userinfo enable")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err);
                            request.body.companyTitle = doc.title;
                            next();
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCompanys_Delete_User = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var users = [];
        (request.body.users != null)? users.$in = request.body.users : null;


        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        (request.body.users != null)? push.users = users : null;
        data.$push = push;

        infomations_companys
            .findOneAndUpdate(query,data,{new:true})
            .populate("users.by","username userinfo enable")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err);
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};



exports.onCompanys_Add_UserSendEmail = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Add_UserSendEmail";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var users = {};
        (request.body.users != null)? users.$each = request.body.users : null;


        for(i=0; i<request.body.users.length; i++){
            var user = request.body.users[i];
            profile.onProfileById(user.by, function (res) {
                console.log(res);
                var message = "เราต้องการให้คุณเข้าร่วเป็นส่วนหนึ่งกับ "+request.body.companyTitle+" เพื่อพัฒนาการให้บริการ ";
                var subject = "Invite Service";
                utils.sendEmailService(res.userinfo.email,subject, message, function (error, info) {

                })
            });
        }

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};