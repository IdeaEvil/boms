var utils = require('../../../../helpers/utils');
var infomations_jobs = require('../models/config_jobtypes.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

exports.onQuery_Jobs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Jobs";
    var query = {};
    query.enable = true;
    (request.params.id != null) ? query.company = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_jobs
            .find(query)
            .lean()
            .exec(function (err, doc) {

                for(i=0; i<doc.length; i++){
                    delete doc[i].update;
                    delete doc[i].create;
                    delete doc[i].__v
                }

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCreate_Jobs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Jobs";
    try {

        var create = {};
        // create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.key != null) ? data.key = request.body.key : null;

        data.create = create;

        var infomations_job = new infomations_jobs(data);
        infomations_job.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_Jobs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_Jobs";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.key != null) ? data.key = request.body.key : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        // update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomations_jobs
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                delete doc.update;
                delete doc.create;
                delete doc.__v;

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};