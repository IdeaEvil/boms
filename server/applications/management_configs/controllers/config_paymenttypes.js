var utils = require('../../../../helpers/utils');
var config_paymrnttypes = require('../models/config_paymrnttypes.models');
var resMsgs = require('../../management_messages/controllers/Management_Messages');
var mongo = require('mongodb');


exports.onQuery_PaymentTypes = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_PaymentTypes";
    var query = {};
    query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

    try {
        config_paymrnttypes
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_PaymentType = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_PaymentType";
    var query = {};
    query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

    try {
        config_paymrnttypes
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};



exports.onCreate_PaymentTypes = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_PaymentTypes";
    try {

        var create = {};
        // create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.val != null) ? data.val = request.body.val : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        data.create = create;

        var config_paymrnttype = new config_paymrnttypes(data);
        config_paymrnttype.save(function (err, doc) {
            if (!err) {
                resMsgs.onResponse_Messages(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onResponse_Messages(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_PaymentTypes = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_PaymentTypes";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.val != null) ? data.val = request.body.val : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        // update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        config_paymrnttypes
            .findOne(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};