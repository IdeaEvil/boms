'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_PricesSchema  = new Schema({
    company         : [{type: Schema.ObjectId, ref: 'Infomation_Companys'}],
    type			: [{
        name        : {type: String, default: null},
        enable      : {type: Boolean, default: true},
        description : {type: String, default: null},
        val         : {type: Number, default: 0}
    }],
    create          : {
        by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date        : {type: Date, default:  Date.now}
    },
    update          : [{
        by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date        : {type: Date, default:  Date.now}
    }]

});

var Infomation_Prices = mongoose.model('Infomation_Prices', Infomation_PricesSchema, 'Infomation_Prices');
module.exports = Infomation_Prices;
