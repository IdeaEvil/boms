var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var multers = require('../../../../helpers/multer');
var distant = require('../../../../helpers/google/util_distance')

var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');
var multer = require('multer');
var format = require('string-format');
format.extend(String.prototype, {});

var storage = multers.onConfigMulter(configs.images_part.promotion);
var upload = multer({ storage : storage }).single('image');

var infomation_jobsheets = require('../models/infomation_jobsheets.models');

exports.onSumDimension = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreateJobs";

    items = request.body.job_position;
    var total_distant = 0;

    console.log(items.length);
    var total = [];
    if(items.length > 1){
        for (i in items){
            // console.log(items[i].locations.lat+","+items[i].locations.long);
            if(i < (items.length-1)){
                var origin = "{0},{1}".format(items[i].locations.lat,items[i].locations.long);
                var index  = parseInt(i)+1;
                var destination = "{0},{1}".format(items[index].locations.lat,items[index].locations.long);

                distant.onDistance(origin, destination, function (data) {
                    total.push(data.distanceValue);

                    if(total.length == i){
                        for(j in total){
                            total_distant = total_distant+total[j];
                        }
                        request.body.total_distance = total_distant;
                        return next();

                    }
                });

            }
        }
    }else{
        request.server.total_distance = 0;
        // console.log(total_distant);
    }
}

// exports.onCheckSubPrice = function (request, response, next) {
//     var startTime = utils.getTimeInMsec();
//     var command = "onCheckSubPrice";
//     try {
//
//         // var query = {};
//         // (request.body.car_of_service != null) ? query._id = new mongo.ObjectID(request.body.car_of_service) : null;
//
//         // request.body.car_of_service = "5b6fe19b824fb8640efb358a";
//
//         Management_CarType.onCheckConfigPrice(request.body.car_of_service,function (err,res) {
//             console.log(res.service);
//             items = res.service;
//             for (i in items){
//                 if(request.body.total_distance > items[i].distance){
//                     request.body.total_price = items[i].km_price*(request.body.total_distance/1000);
//                     next();
//                 }
//             }
//         })
//
//
//
//     } catch (err) {
//         resMsgs.onResponse_Messages(0,40400,function(res){
//             response.status(404).json(res);
//             utils.writeLog(command, request, startTime, res, err)
//         });
//     }
//
// }

exports.onQueryJobs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQueryJobs";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomation_jobsheets
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onCreateJobs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreateJobs";

    try {

        var data = {};

        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.ref != null) ? data.ref = request.body.ref : null;
        (request.body.car != null) ? data.car = request.body.car : null;
        (request.body.type != null) ? data.v = request.body.type : null;
        // (request.body.job_start != null) ? data.job_start = request.body.job_start : null;

        // (request.body.enable != null) ? data.enable = request.body.enable : null; // for system

        (request.body.total_distance != null) ? data.total_distance = request.body.total_distance : null; // for system
        (request.body.total_price != null) ? data.total_price = request.body.total_price : null; // for system
        (request.body.total_dimension != null) ? data.total_dimension = request.body.total_dimension : null;

        (request.body.promotions != null) ? data.promotions = request.body.promotions : null;
        (request.body.payment_channel != null) ? data.payment_channel = request.body.payment_channel : null;

        (request.body.start != null) ? data.start = request.body.start : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        (request.body.job_position != null) ? data.job_position = request.body.job_position : null;

        (request.body.benefit != null) ? data.benefit = request.body.benefit : null;
        (request.body.workers != null) ? data.workers = request.body.workers : null;

        var create = {};
        create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        data.create = create;

        var infomation_jobsheet = new infomation_jobsheets(data);
        infomation_jobsheet.save(function (err, doc) {
            // delete doc.update;
            // delete doc.create;
            // delete doc.benefit;
            // delete doc.payment;
            // delete doc.dimension;

            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                console.log(err);
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog('checkVersion', request, startTime, res, err)
                });
            }
        });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.on๊UpdateJobs = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "on๊UpdateJobs";

    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.ref != null) ? data.ref = request.body.ref : null;
        (request.body.car != null) ? data.car = request.body.car : null;
        (request.body.type != null) ? data.v = request.body.type : null;
        // (request.body.job_start != null) ? data.job_start = request.body.job_start : null;

        // (request.body.enable != null) ? data.enable = request.body.enable : null; // for system

        (request.body.total_distance != null) ? data.total_distance = request.body.total_distance : null; // for system
        (request.body.total_price != null) ? data.total_price = request.body.total_price : null; // for system
        (request.body.total_dimension != null) ? data.total_dimension = request.body.total_dimension : null;

        (request.body.promotions != null) ? data.promotions = request.body.promotions : null;
        (request.body.payment_channel != null) ? data.payment_channel = request.body.payment_channel : null;

        (request.body.start != null) ? data.start = request.body.start : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        (request.body.job_position != null) ? data.job_position = request.body.job_position : null;

        (request.body.benefit != null) ? data.benefit = request.body.benefit : null;
        (request.body.workers != null) ? data.workers = request.body.workers : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;


        infomation_jobsheets
            .findOneAndUpdate(query, data, { new: true })
            .lean()
            .exec(function (err, doc) {
                delete doc.update;
                delete doc.create;
                delete doc.benefit;
                if(!err){
                    resMsgs.onMessage_Response(0,20000,function(res){
                        var resData = res;
                        resData.data = doc;
                        response.status(200).json(resData);
                        utils.writeLog(command, request, startTime, resData, err)
                    });
                }else{
                    resMsgs.onMessage_Response(0,50003,function(res){
                        response.status(500).json(res);
                        utils.writeLog('checkVersion', request, startTime, res, err)
                    });
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}