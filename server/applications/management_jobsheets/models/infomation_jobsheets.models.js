'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomations_JobSheetsSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    ref                 : [{
        title           : {type: String, default: null},
        code            : {type: String, default: null},
        description     : {type: String, default: null},
    }],
    cars                : [{type: Schema.ObjectId, ref: 'Infomations_Cars'}],
    type                : {
        name            : {type: String, default:null},
        key             : {type: Number, default: 0.0},
        description     : {type: String, default: null},
    },  // roundtrip, reservation, install

    total_distance      : {type: Number, default:0.0},
    total_price         : {type: Number, default:0.0},
    total_dimension     : {
        weight          : {type: Number, default:0.0},
        long            : {type: Number, default:0.0},
        height          : {type: Number, default:0.0},
    },

    promotions          : {type: Schema.ObjectId, ref: 'Management_Promotions_Card'},
    payment_channel     : {type: Schema.ObjectId, ref: 'Payment_Channels'},

    start               : {type: Date, default:null},
    enable              : {type: Boolean, default: true},

    job_position           : [{
        title           : {type: String, default:null},
        description     : {type: String, default: null},
        contact         : {
            name        : {type: String, default:null},
            msisdn      : {type: String, default:null},
            note        : {type: String, default:null},
        },
        locations       : {
            address     : {type: String, default:null},
            lat         : {type: String, default:0.0},
            long        : {type: String, default:0.0},
        },
        addon           : [{type: Schema.ObjectId, ref: 'Job_Services'}],
        controll        : {
            start       : {type: Date, default:null},
            end         : {type: Date, default:null},
            images      : [{
                image   : {type: String, default:null},
            }],
            signature   : {type: String, default:null},
            note        : {type: String, default:null},
            locations   : {
                lat     : {type: String, default:0.0},
                long    : {type: String, default:0.0},
            }
        },
        products        : [{
            name        : {type: String, default:null},
            taxid       : {type: String, default:null},
            number      : {type: Number, default:0.0},
            checker     : {type: Boolean, default:true},
        }],
    }],

    benefit             : {
        total           : {type: Number, default:0.0},
        companey        : {type: Number, default:0.0},
        expense         : {type: Number, default:0.0},
        system          : {type: Number, default:0.0}
    },

    workers             : [{
        type            : {type: Schema.ObjectId, ref: 'Job_Worker_Types'},
        allowance       : {type: Number, default:0.0},
        carinfo         : {type: Schema.ObjectId, ref: 'Car_Infomations'},
        by              : {type: Schema.ObjectId, ref: 'Infomation_Profiles'},
        date            : {type: Date, default:  Date.now}
    }],

    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Infomations_JobSheets = mongoose.model('Infomations_JobSheets', Infomations_JobSheetsSchema, 'Infomations_JobSheets');
module.exports = Infomations_JobSheets;