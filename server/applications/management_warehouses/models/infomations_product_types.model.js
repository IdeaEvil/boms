'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomations_Product_TypesSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Infomations_Product_Types = mongoose.model('Infomations_Product_Types', Infomations_Product_TypesSchema, 'Infomations_Product_Types');
module.exports = Infomations_Product_Types;