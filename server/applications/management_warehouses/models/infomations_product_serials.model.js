'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_Product_SerialsSchema  = new Schema({
    company                 : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    support                 : [{
        partners            : {type: Schema.ObjectId, ref: 'Infomation_Partners'},
        charged_product     : {
            import_value    : {type: Number, default:0.0},
            daily_freight   : {type: Number, default:0.0},
            export_value    : {type: Number, default:0.0},
            transportation  : {type: Number, default:0.0},
        },
    }],
    dimension               : {
        width               : {type: Number, default:0.0},
        long                : {type: Number, default:0.0},
        height              : {type: Number, default:0.0},
        description         : {type: Number, default:0.0},
    },
    serial_product          : {type: String, default:null},
    title                   : {type: String, default:null},
    description             : {type: String, default: null},
    image                   : {type: String, default: null},
    type                    : {type: Schema.ObjectId, ref: 'Infomations_Product_Types'},
    enable                  : {type: Boolean, default: true},
    create                  : {
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    },
    update                  : [{
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    }]
});

var Infomation_Product_Serials = mongoose.model('Infomation_Product_Serials', Infomation_Product_SerialsSchema, 'Infomation_Product_Serials');
module.exports = Infomation_Product_Serials;