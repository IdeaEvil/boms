var utils = require('../../../../helpers/utils');
var infomations_product_serials = require('../models/infomations_product_serials.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

exports.onQuery_Product_Serials = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Product_Serials";
    var query = {};
    // query.enable = true;

    try {
        infomations_product_serials
            .find(query)
            .populate("support.partners","title contact enable invoice")
            .populate("type","title description enable")
            .sort("serial_product")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Product_Serial = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Product_Serial";

    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_product_serials
            .findOne(query)
            .populate("support.partners","title contact enable invoice")
            .populate("type","title description enable")
            .sort("serial_product")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Product_SerialAndPartners = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Product_Serial";

    var query_array = {};
    (request.params.partners != null) ? query_array.partners = request.params.partners : null;

    var items = {};
    items.$elemMatch = query_array;

    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
    query.support = items;

    try {
        infomations_product_serials
            .findOne(query)
            .populate("support.partners","title contact enable invoice")
            .populate("type","title description enable")
            .sort("serial_product")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Product_SerialByPartners = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Product_Serial";

    var query_array = {};
    (request.params.partners != null) ? query_array.partners = request.params.partners : null;

    var items = {};
    items.$elemMatch = query_array;

    var query = {};
    // query.enable = true;
    // (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
    query.support = items;

    try {
        infomations_product_serials
            .find(query)
            .populate("support.partners","title contact enable invoice")
            .populate("type","title description enable")
            .sort("serial_product")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCreate_Product_Serial = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Product_Serial";
    try {

        var create = {};
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;
        create.date = startTime;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.serial_product != null) ? data.serial_product = request.body.serial_product : null;
        (request.body.dimension != null) ? data.dimension = request.body.dimension : null;
        (request.body.support != null) ? data.support = request.body.support : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.type != null) ? data.type = request.body.type : null;

        data.create = create;

        var infomations_product_serial = new infomations_product_serials(data);
        infomations_product_serial.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_Product_Serial = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_Product_Serial";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.serial_product != null) ? data.serial_product = request.body.serial_product : null;
        (request.body.dimension != null) ? data.dimension = request.body.dimension : null;
        (request.body.support != null) ? data.support = request.body.support : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.type != null) ? data.type = request.body.type : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomations_product_serials
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onProduct_Serial_AddSupport = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onProduct_Serial_AddSupport";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var support = {};
        (request.body.support != null)? support.$each = request.body.support : null;


        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        (request.body.support != null)? push.support = support : null;
        data.$push = push;


        infomations_product_serials
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onProduct_Serial_DeleteSupport = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        // var support = [];
        // (request.body.support != null)? support.$in = request.body.support : null;


        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        var pull = {};
        push.update =  update;
        (request.body.support != null)? pull.support = request.body.support : null;
        data.$pull = pull;
        data.$push = push;


        infomations_product_serials
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                console.log(err);
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
        //
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onCheck_ProductSerialSupport = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCheck_WarehouseSupport";

    var query_array = {};
    query_array.partners = request.body.partner.import;

    var support = {};
    support.$elemMatch = query_array;

    var query = {};
    query.support = support;
    query.company = new mongo.ObjectID(request.body.company);
    query.serial_product = request.body.serial_product;

    try {
        infomations_product_serials
            .findOne(query)
            .lean()
            .exec(function (err, doc) {

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        request.body.serial_product = doc._id;
                        var data = doc.support;
                        for(i=0; i<data.length; i++){
                            if(request.body.partner.import == data[i].partners){
                                request.body.charged_product = data[i].charged_product;
                                next();
                            }
                        }
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};