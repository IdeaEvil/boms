var utils = require('../../../../helpers/utils');
var infomations_product_types = require('../models/infomations_product_types.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

exports.onQuery_Product_Types = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Product_Types";
    var query = {};
    query.enable = true;

    try {
        infomations_product_types
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Product_Type = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Product_Types";
    var query = {};
    query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_product_types
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCreate_Product = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Product";
    try {

        var create = {};
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;
        // create.date = startTime;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        data.create = create;

        var infomations_product_type = new infomations_product_types(data);
        infomations_product_type.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_Product = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_Product";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomations_product_types
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};